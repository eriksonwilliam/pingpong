package main

import (
	"fmt"
	"time"
)

func ping(c chan string) {
	for i := 0; ; i++ {
		c <- "ping"
	}
}

func pong(c chan string) {
	for i := 0; ; i++ {
		c <- "pong"
	}
}

func main() {
	var c chan string = make(chan string)

	for i := 0; ; i++ {
		if i%2 == 0 {
			go ping(c)
			msg := <-c
			fmt.Println(msg)
			time.Sleep(time.Second * 1)
		} else {
			go pong(c)
			msg := <-c
			fmt.Println(msg)
			time.Sleep(time.Second * 1)
		}
	}

}
